jQuery(document).ready(function () {
  var floatingDiv, detailsPos;
  getFloatEle();

  function getFloatEle() {
    floatingDiv = jQuery(".qbr-finePrint-nav-fixed-main"), detailsPos = floatingDiv.offset().top;
  }

  function setNavPosition() {
    var bottomP = jQuery(window).scrollTop();
    if (bottomP < detailsPos) {
      floatingDiv.removeClass('is-fixed');
    } else {
      floatingDiv.addClass('is-fixed');
    }
  }
  jQuery('a.back-to-top').click(function () {
    jQuery(".qbr-finePrint-nav-join-nav").slideUp();
    jQuery('html, body').animate({
      scrollTop: 0
    }, 700);
    return false;
  });

  var sec1 = jQuery('.qbr-finePrints-Sec-1');
  var sec2 = jQuery('.qbr-finePrints-Sec-2');
  var sec3 = jQuery('.qbr-finePrints-Sec-3');
  var sec4 = jQuery('.qbr-finePrints-Sec-4');
  var sec5 = jQuery('.qbr-finePrints-Sec-5');

  var anchorLinks = jQuery('.qbr-anchor-link');

  var navDesktop = jQuery('qbr-nav-desktop');
  var navMobile = jQuery('qbr-nav-mobile');

  jQuery(window).on("scroll", function () {
    setNavPosition();

    var amountScrolled = 200;

    if (jQuery(window).scrollTop() > amountScrolled) {
      jQuery('a.back-to-top').fadeIn('slow');
    } else {
      jQuery('a.back-to-top').fadeOut('slow');
    }

    var docScrollTop = jQuery(document).scrollTop();

    var sec1Offset = jQuery('.qbr-finePrints-Sec-1').offset().top - docScrollTop;
    var sec2Offset = jQuery('.qbr-finePrints-Sec-2').offset().top - docScrollTop;
    var sec3Offset = jQuery('.qbr-finePrints-Sec-3').offset().top - docScrollTop;
    var sec4Offset = jQuery('.qbr-finePrints-Sec-4').offset().top - docScrollTop;
    var sec5Offset = jQuery('.qbr-finePrints-Sec-5').offset().top - docScrollTop;

    if (sec1Offset < 200) {
      anchorLinks.removeClass('qbr-finePrint-selectLink');
      anchorLinks.eq(0).addClass('qbr-finePrint-selectLink');
      anchorLinks.eq(5).addClass('qbr-finePrint-selectLink');
    }
    if (sec2Offset < 150) {
      anchorLinks.removeClass('qbr-finePrint-selectLink');
      anchorLinks.eq(1).addClass('qbr-finePrint-selectLink');
      anchorLinks.eq(6).addClass('qbr-finePrint-selectLink');
    }
    if (sec3Offset < 150) {
      anchorLinks.removeClass('qbr-finePrint-selectLink');
      anchorLinks.eq(2).addClass('qbr-finePrint-selectLink');
      anchorLinks.eq(7).addClass('qbr-finePrint-selectLink');
    }
    if (sec4Offset < 150) {
      anchorLinks.removeClass('qbr-finePrint-selectLink');
      anchorLinks.eq(3).addClass('qbr-finePrint-selectLink');
      anchorLinks.eq(8).addClass('qbr-finePrint-selectLink');
    }
    if (sec5Offset < 150) {
      anchorLinks.removeClass('qbr-finePrint-selectLink');
      anchorLinks.eq(4).addClass('qbr-finePrint-selectLink');
      anchorLinks.eq(9).addClass('qbr-finePrint-selectLink');
      return;
    }
  });

  jQuery(window).resize(function () {
    floatingDiv.removeClass('is-fixed');
    getFloatEle();
    setNavPosition();
  });

  // setup anchor scroll

  anchorLinks.click(function (e) {
    var targetName = jQuery(this).attr('data-target');
    var offset = jQuery(window).width() < 960 ? navMobile.height() + 60 : navDesktop.height() + 100;

    jQuery(".qbr-finePrint-nav-join-nav").hide();
    jQuery('html, body').animate({
      scrollTop: jQuery('#' + targetName).offset().top - offset
    }, 1000);
  });
});

jQuery("#qbr-finePrint-nav-join").click(function () {
  jQuery(".qbr-finePrint-nav-join-nav").slideToggle();
  jQuery(".qbr-finePrint-nav-chev").toggleClass("transform");
});

/** Tabs component **/

!function (a, n) {
  "function" == typeof define && define.amd ? define(n) : "object" == typeof exports ? module.exports = n(require, exports, module) : a.CountUp = n();
}(this, function (a, n, t) {
  var e = function (a, n, t, e, i, r) {
    function o(a) {
      var n,
          t,
          e,
          i,
          r,
          o,
          s = a < 0;if (a = Math.abs(a).toFixed(l.decimals), a += "", n = a.split("."), t = n[0], e = n.length > 1 ? l.options.decimal + n[1] : "", l.options.useGrouping) {
        for (i = "", r = 0, o = t.length; r < o; ++r) 0 !== r && r % 3 === 0 && (i = l.options.separator + i), i = t[o - r - 1] + i;t = i;
      }return l.options.numerals.length && (t = t.replace(/[0-9]/g, function (a) {
        return l.options.numerals[+a];
      }), e = e.replace(/[0-9]/g, function (a) {
        return l.options.numerals[+a];
      })), (s ? "-" : "") + l.options.prefix + t + e + l.options.suffix;
    }function s(a, n, t, e) {
      return t * (-Math.pow(2, -10 * a / e) + 1) * 1024 / 1023 + n;
    }function u(a) {
      return "number" == typeof a && !isNaN(a);
    }var l = this;if (l.version = function () {
      return "1.9.3";
    }, l.options = { useEasing: !0, useGrouping: !0, separator: ",", decimal: ".", easingFn: s, formattingFn: o, prefix: "", suffix: "", numerals: [] }, r && "object" == typeof r) for (var m in l.options) r.hasOwnProperty(m) && null !== r[m] && (l.options[m] = r[m]);"" === l.options.separator ? l.options.useGrouping = !1 : l.options.separator = "" + l.options.separator;for (var d = 0, c = ["webkit", "moz", "ms", "o"], f = 0; f < c.length && !window.requestAnimationFrame; ++f) window.requestAnimationFrame = window[c[f] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[c[f] + "CancelAnimationFrame"] || window[c[f] + "CancelRequestAnimationFrame"];window.requestAnimationFrame || (window.requestAnimationFrame = function (a, n) {
      var t = new Date().getTime(),
          e = Math.max(0, 16 - (t - d)),
          i = window.setTimeout(function () {
        a(t + e);
      }, e);return d = t + e, i;
    }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function (a) {
      clearTimeout(a);
    }), l.initialize = function () {
      return !!l.initialized || (l.error = "", l.d = "string" == typeof a ? document.getElementById(a) : a, l.d ? (l.startVal = Number(n), l.endVal = Number(t), u(l.startVal) && u(l.endVal) ? (l.decimals = Math.max(0, e || 0), l.dec = Math.pow(10, l.decimals), l.duration = 1e3 * Number(i) || 2e3, l.countDown = l.startVal > l.endVal, l.frameVal = l.startVal, l.initialized = !0, !0) : (l.error = "[CountUp] startVal (" + n + ") or endVal (" + t + ") is not a number", !1)) : (l.error = "[CountUp] target is null or undefined", !1));
    }, l.printValue = function (a) {
      var n = l.options.formattingFn(a);"INPUT" === l.d.tagName ? this.d.value = n : "text" === l.d.tagName || "tspan" === l.d.tagName ? this.d.textContent = n : this.d.innerHTML = n;
    }, l.count = function (a) {
      l.startTime || (l.startTime = a), l.timestamp = a;var n = a - l.startTime;l.remaining = l.duration - n, l.options.useEasing ? l.countDown ? l.frameVal = l.startVal - l.options.easingFn(n, 0, l.startVal - l.endVal, l.duration) : l.frameVal = l.options.easingFn(n, l.startVal, l.endVal - l.startVal, l.duration) : l.countDown ? l.frameVal = l.startVal - (l.startVal - l.endVal) * (n / l.duration) : l.frameVal = l.startVal + (l.endVal - l.startVal) * (n / l.duration), l.countDown ? l.frameVal = l.frameVal < l.endVal ? l.endVal : l.frameVal : l.frameVal = l.frameVal > l.endVal ? l.endVal : l.frameVal, l.frameVal = Math.round(l.frameVal * l.dec) / l.dec, l.printValue(l.frameVal), n < l.duration ? l.rAF = requestAnimationFrame(l.count) : l.callback && l.callback();
    }, l.start = function (a) {
      l.initialize() && (l.callback = a, l.rAF = requestAnimationFrame(l.count));
    }, l.pauseResume = function () {
      l.paused ? (l.paused = !1, delete l.startTime, l.duration = l.remaining, l.startVal = l.frameVal, requestAnimationFrame(l.count)) : (l.paused = !0, cancelAnimationFrame(l.rAF));
    }, l.reset = function () {
      l.paused = !1, delete l.startTime, l.initialized = !1, l.initialize() && (cancelAnimationFrame(l.rAF), l.printValue(l.startVal));
    }, l.update = function (a) {
      if (l.initialize()) {
        if (a = Number(a), !u(a)) return void (l.error = "[CountUp] update() - new endVal is not a number: " + a);l.error = "", a !== l.frameVal && (cancelAnimationFrame(l.rAF), l.paused = !1, delete l.startTime, l.startVal = l.frameVal, l.endVal = a, l.countDown = l.startVal > l.endVal, l.rAF = requestAnimationFrame(l.count));
      }
    }, l.initialize() && l.printValue(l.startVal);
  };return e;
});

var currentLevel = '';
var selectedLevel = '';
var easingFn = function (t, b, c, d) {
  var ts = (t /= d) * t;
  var tc = ts * t;
  return b + c * (tc * ts + -5 * ts * ts + 10 * tc + -10 * ts + 5 * t);
};
var options = {
  useEasing: true,
  easingFn: easingFn,
  useGrouping: false,
  separator: ',',
  decimal: '.'
};

if (window.location.hash == '#level1' || window.location.hash == '#level2' || window.location.hash == '#level3') {
  currentLevel = selectedLevel = window.location.hash.replace("#", "");
  jQuery('.partner_section2_membershiplevels_tab_item.level' + window.location.hash.replace("#", "")).addClass('active');
  jQuery('[data-level="#level' + window.location.hash.replace("#", "") + '"]').show();
} else {
  currentLevel = selectedLevel = 1;
  jQuery('.partner_section2_membershiplevels_tab_item.level1').addClass('active');
  jQuery('[data-level="#level1"]').show();
}

function selectTab(level) {

  selectedLevel = level.replace("level", "");
  if (currentLevel == selectedLevel) return;
  var lvl1down = new CountUp('level1pts', 30, 20, 0, 1, options);
  var lvl2 = new CountUp('level2pts', 20, 30, 0, 1, options);
  var lvl2down = new CountUp('level2pts', 40, 30, 0, 1, options);
  var lvl3 = new CountUp('level3pts', 30, 40, 0, 1, options);

  var lvl1pdown = new CountUp('level1pct', 7, 5, 0, 1, options);
  var lvl2p = new CountUp('level2pct', 5, 7, 0, 1, options);
  var lvl2pdown = new CountUp('level2pct', 8, 7, 0, 1, options);
  var lvl3p = new CountUp('level3pct', 7, 8, 0, 1, options);

  if (selectedLevel > currentLevel) {
    lvl2.reset();
    lvl2.start();
    lvl3.reset();
    lvl3.start();

    lvl2p.reset();
    lvl2p.start();
    lvl3p.reset();
    lvl3p.start();
  } else if (selectedLevel < currentLevel) {
    lvl1down.reset();
    lvl1down.start();
    lvl2down.reset();
    lvl2down.start();

    lvl1pdown.reset();
    lvl1pdown.start();
    lvl2pdown.reset();
    lvl2pdown.start();
  }
  currentLevel = selectedLevel;

  jQuery('.partner_section2_membershiplevels_tab_item').removeClass('active');
  jQuery('.partner_section2_membershiplevels_tab_item.' + level).addClass('active');
  jQuery('[data-level]').hide();
  jQuery('[data-level="#' + level + '"]').show();
}